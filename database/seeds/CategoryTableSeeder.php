<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $cats = ['Chinese Food', 'Thai Food' ,'Korea Food'];

        for ($i=0; $i < sizeof($cats); $i++) { 
          Category::create([
            'name' => $cats[$i]
          ]);       
        }
    }
}
