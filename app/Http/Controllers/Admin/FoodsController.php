<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Food;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Category;

class FoodsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $foods = Food::with('category')->paginate(15);
        return view('admin.foods.index', compact('foods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {   
        $categories = $this->getCategoryList();
        return view('admin.foods.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        Food::create($request->all());

        Session::flash('flash_message', 'Food added!');

        return redirect('admin/foods');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $food = Food::findOrFail($id);

        return view('admin.foods.show', compact('food'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {   
        $categories = $this->getCategoryList();
        $food = Food::findOrFail($id);

        return view('admin.foods.edit', compact('food','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $food = Food::findOrFail($id);
        $food->update($request->all());

        Session::flash('flash_message', 'Food updated!');

        return redirect('admin/foods');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Food::destroy($id);

        Session::flash('flash_message', 'Food deleted!');

        return redirect('admin/foods');
    }

    public function getCategoryList()
    {
        return Category::lists('name', 'id');
    }

}
