<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cart;
use App\Food;
use App\Order;

class OrderController extends Controller
{


    public function addCart(Request $request)
    {   

        $food_id = $request->get('food_id');
        $food = Food::find($food_id);
        Cart::add(array('id' => $food_id, 'name' => $food->name, 'qty' => 1, 'price' => $food->price));

         return redirect('/');
    }


    public function checkOut() {

     $cart = Cart::content();
     return view('frontend.cart',['cart' => $cart]);

    }

    public function cartQtyUp(Request $request)
    {   
        //cart qty up
                
    }

    public function cartQtyDown(Request $request)
    {
       //cart qty down 
    }

    public function cartItemRemove(Request $request)
    {
      //remove cart item    
    }


    public function handleOrder(Request $request)
    { 
      $orderItems = Cart::content();
      $user = \Auth::user();
      $food= new Food;
      $order = Order::create([
                'user_id'  => $user->id,
                'name'     => $request->get('name'),
                'email'    => $request->get('email'),
                'contact'  => $request->get('contact'),
                'address'  => $request->get('address'),
                ]);
  
     foreach ($orderItems as $item) {
   
      $order->foods()->attach($item['id'], ['qty' => $item['qty'],'price' => $item['price'], 'subtotal' => $item['subtotal'] ]);

     }

     Cart::destroy();

     return redirect('/');
    }




}
