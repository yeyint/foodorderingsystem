<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Food;
use App\Category;
use Cart;

class FoodController extends Controller
{   
    protected $model;


    public function __construct(Food $food) {

        $this->model = $food;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $foods         = $this->model->paginate(20);
        $categories    = Category::get();
        $cart          = Cart::content();

        return view('home',compact('foods','categories','cart'));
    }



   
}
