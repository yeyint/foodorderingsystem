<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table="orders";

    protected $fillable = ['user_id','name','email','contact','address','status'];

    /**
     * order belongsto user
     * @return order user
     */
    public function user()
    {
      return $this->belongsTo('App\User');
    }

    /**
     * Order has many food
     * 
     */
    public function foods()
    {
      return $this->belongsToMany('App\Food','food_order', 'food_id', 'order_id')->withPivot( 'qty','price', 'subtotal')->withTimestamps();
    }
}
