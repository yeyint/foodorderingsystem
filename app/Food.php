<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Food extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'foods';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','price', 'category_id'];

    /**
     * 
     *category relation
     */
    public function category()
    {
       return $this->belongsTo(Category::class);
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order','food_order', 'food_id', 'order_id')->withPivot('qty','price', 'subtotal')->withTimestamps();
    }

}
