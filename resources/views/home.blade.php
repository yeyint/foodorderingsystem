@extends('layouts.app')

@if(Auth::guest())
  @section('content')

   @include('welcome')
   
  @endsection

@else
  @section('content')
  <div class="container-fluid"> 
     <div class="row">
        <div class="col-md-2">
           <div class="panel panel-default">
             <div class="panel-heading">
                Food Categorirs
             </div>
             <div class="panel-body">
                 <ul class="list-group">
                    @foreach ($categories as $category)
                     <li class="list-group-item">
                     <i class="fa fa-spoon"></i>
                     <a href="#">{{ $category['name'] }}</a></li>
                    @endforeach
                 </ul>
             </div>
           </div>
        </div> 
        <div class="col-md-10">
           <div class="panel panel-default">
             <div class="panel-body">
               <div class="row">
                 @foreach ($foods as $food) 
                  <div class="col-md-3">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                         
                        {{ $food['price']}} -KS
                      </div>
                      <div class="panel-body">
                          <p>{{ $food['name'] }}</p>
                         <img class="img-responsive" src="{{ asset('assets/img/noddle.jpg') }}">
                      </div>
                      <div class="panel-footer">
                         @include('frontend.partials.addtocart', array('data' => $food))
                      </div>
                    </div>
                  </div>
                 @endforeach
               </div>
             </div>
           </div>

         
        </div>
     </div>
  </div>
  @endsection
@endif
