@extends('layouts.app')

@section('content')       
<section id="cart_items">
    <div class="container">
        <a href="{{ url('/') }}" class="btn btn-info">
        <i class="fa fa-cart-plus"></i>
         Continue Order</a>
        <hr/>
        <div class="table-responsive cart_info">
            @if(count($cart))
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="">No</td>
                        <td class="name">Name</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                   {{-- */$x=0;/* --}}
                    @foreach($cart as $item)
                    {{-- */$x++;/* --}}
                    <tr>
                        <td>{{ $x }}</td>
                        <td class="cart_description">
                            <h4><a href="">{{$item->name}}</a></h4>
                        </td>
                        <td class="cart_price">
                            <p>${{$item->price}}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up btn btn-default btn-sm" href=""> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="{{$item->qty}}" autocomplete="off" size="2">
                                <a class="cart_quantity_down btn btn-default btn-sm" 
                                href=""> - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">{{$item->subtotal}} -KS</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    
                    @endforeach
                    @else
                    <p>No items in your cart</p>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
      <div class="row">
          <div class="col-md-6">
            {!! Form::open(['url' => '/cart/order', 'method' => 'POST']) !!}
                <div class="form-group ">
                    <label for="">Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Name" required>
                </div>
                <div class="form-group ">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" required></input>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea name="contact" class="form-control" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label>Delivery Address</label>
                    <textarea name="address" class="form-control" rows="3"></textarea>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit Order</button>
                </div>
            </form>
          </div>
          <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul>
                        <li>Discount <span>15%</span></li>
                        <li>Total <span>${{Cart::total()}}</span></li>
                    </ul>
                    <a class="btn btn-default update pull-right" href="">Clear Cart</a>
                </div>
            </div>
          </div>
      </div>
    </div>
</section>
@endsection