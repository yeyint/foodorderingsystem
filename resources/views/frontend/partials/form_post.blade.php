{!! Form::open(['url' => 'cart','method' => 'POST']) !!}
  
  {!! Form::hidden('food_id',$data['id']) !!} 
  {!! Form::submit('AddToCart', ['class' => 'btn btn-default btn-sm']) !!}

{!! Form::close() !!}