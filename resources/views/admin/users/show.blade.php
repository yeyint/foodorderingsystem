@extends('layouts.backend',['name'  => '%%crudName%%'])

@section('content')
    <div class="header">
      <a href="{{ url('%%routeGroup%%%%crudName%%') }}" class="btn btn-primary"><i class="fa fa-reply"></i> Back</a> 
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Email</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $user->id }}</td> <td> {{ $user->name }} </td><td> {{ $user->email }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection