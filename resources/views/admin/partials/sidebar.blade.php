<div class="panel panel-default">
  <div class="panel-body">
     <ul class="list-group">
       <li class="list-group-item">
           <a href="{{ url('/admin/dashboard') }}"> <i class="fa fa-btn fa-dashboard"> Dashboard</i> </a>
       </li>
       <li class="list-group-item">
           <a href="{{ url('/admin/foods') }}"> <i class="fa fa-btn fa-spoon"></i> Food </a>
       </li>
       <li class="list-group-item">
           <a href="{{ url('/admin/categories') }}"> <i class="fa fa-btn fa-cubes"></i> Category </a>
       </li>
       <li class="list-group-item">
           <a href="{{ url('admin/orders') }}"> <i class="fa fa-btn fa-play-circle"></i> Order </a>
       </li>
       <li class="list-group-item">
           <a href="{{ url('admin/users') }}"> <i class="fa fa-btn fa-users"></i> Members </a>
       </li>
     </ul>
  </div>
</div>
