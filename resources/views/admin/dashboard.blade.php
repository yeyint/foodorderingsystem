@extends('layouts.backend',['name' => 'dashboard'])


@section('content')
  
<div class="row">
  <div class="col-md-4">
    <div class="panel panel-primary">
      <div class="panel-heading">
        
      </div>
      <div class="panel-body">
        Basic panel example
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-warning">
      <div class="panel-heading">
        
      </div>
      <div class="panel-body">
        Basic panel example
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-danger">
      <div class="panel-heading">
        
      </div>
      <div class="panel-body">
        Basic panel example
      </div>
    </div>
  </div>
</div>
  
@endsection