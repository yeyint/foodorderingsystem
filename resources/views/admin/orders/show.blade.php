@extends('layouts.backend',['name'  => 'Orders'])

@section('content')
    <div class="header">
      <a href="{{ url('admin/orders') }}" class="btn btn-primary"><i class="fa fa-reply"></i> Back</a> 
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> 
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $order->id }}</td> 
                    <td> {{ $order->name }} </td>
                    <td> {{ $order->email }} </td>
                    <td> 
                      @if ($order->status)
                       <p class="text-info">Delivered</p> 
                      @else
                       <p class="text-primary">Ordered</p>
                      @endif
                    </td>
                </tr>
            </tbody>    
        </table>
    </div>
    <div class="table-responsive">
        <h3>Order Foods</h3>
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th> 
                    <th>Price -Ks</th>
                    <th>Qty</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order->foods as $food) 
                <tr>
                    <td>{{ $food->name }}</td> 
                    <td> {{ $food->price }} </td>
                    <td> {{ $food->pivot->qty }} </td>
                    <td> {{ $food->pivot->subtotal }} </td>
                </tr>
                @endforeach
            </tbody>    
        </table>
    </div>

@endsection