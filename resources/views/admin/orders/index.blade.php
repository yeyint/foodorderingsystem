@extends('layouts.backend',['name'  => 'orders'])

@section('content')

    <hr>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>Name</th><th>Email</th><th>Status</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($orders as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td>
                        {{ $item->name }}
                    </td>
                    <td>{{ $item->email }}</td>
                    <td>
                    @if ($item->status)
                    <a href="{{ url('admin/orders/update/status', ['id' => $item->id])}}" class="btn btn-info btn-xs">Delivered</a>
                    @else
                    <a href="{{ url('admin/orders/update/status', ['id' => $item->id])}}" class="btn btn-warning btn-xs">Ordered</a>
                    @endif</td>
                    <td>
                        <a href="{{ url('admin/orders/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/orders', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                        /
                        <a href="{{ url('admin/orders',['id' => $item['id']]) }}" class="btn btn-info btn-xs">View Details</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $orders->render() !!} </div>
    </div>

@endsection
