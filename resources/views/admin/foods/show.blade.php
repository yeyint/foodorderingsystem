@extends('layouts.backend',['name'  => '%%crudName%%'])

@section('content')
    <div class="header">
      <a href="{{ url('%%routeGroup%%%%crudName%%') }}" class="btn btn-primary"><i class="fa fa-reply"></i> Back</a> 
    </div>
    <hr>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Category</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $food->id }}</td> <td> {{ $food->name }} </td><td> {{ $food->category }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection